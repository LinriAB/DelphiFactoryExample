unit FramesUnit;

interface

uses
	Generics.Collections,
	Forms, SysUtils;

type
	TFrameClass = class of TFrame;

var
	Frames : TDictionary<string, TFrameClass>;

implementation

initialization

Frames := TDictionary<string, TFrameClass>.Create;

finalization

FreeAndNil( Frames );

end.
