object MainForm: TMainForm
  Left = 0
  Top = 0
  BorderStyle = bsSingle
  Caption = 'The Main Form'
  ClientHeight = 388
  ClientWidth = 672
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 32
    Top = 32
    Width = 75
    Height = 25
    Caption = 'Frame 1'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 32
    Top = 72
    Width = 75
    Height = 25
    Caption = 'Frame 2'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 32
    Top = 112
    Width = 75
    Height = 25
    Caption = 'Frame 3'
    TabOrder = 2
    OnClick = Button3Click
  end
  object CurrentFramePanel: TPanel
    Left = 136
    Top = 32
    Width = 489
    Height = 313
    Caption = 'CurrentFramePanel'
    ShowCaption = False
    TabOrder = 3
  end
  object Button4: TButton
    Left = 32
    Top = 151
    Width = 75
    Height = 25
    Caption = 'None'
    TabOrder = 4
    OnClick = Button4Click
  end
end
