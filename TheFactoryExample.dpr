program TheFactoryExample;

uses
  Vcl.Forms,
  MainFormUnit in 'MainFormUnit.pas' {MainForm},
  Frame1Unit in 'Frame1Unit.pas' {Frame1: TFrame},
  Frame2Unit in 'Frame2Unit.pas' {Frame2: TFrame},
  Frame3Unit in 'Frame3Unit.pas' {Frame3: TFrame},
  FrameFactoryUnit in 'FrameFactoryUnit.pas',
  FramesUnit in 'FramesUnit.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TMainForm, MainForm);
  Application.Run;
end.
