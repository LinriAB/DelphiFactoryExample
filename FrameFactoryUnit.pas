unit FrameFactoryUnit;

interface

uses
	FramesUnit,

	Forms, SysUtils;

function FrameFactory( AFrameClassName : string ) : TFrame;

implementation

function FrameFactory( AFrameClassName : string ) : TFrame;
var
	FrameClass : TFrameClass;
begin

	// Get the frame class from the collection...
	Frames.TryGetValue( AFrameClassName, FrameClass );

	// ...instantiate it and return the result
	Result := FrameClass.Create( Application.MainForm );

end;

end.
