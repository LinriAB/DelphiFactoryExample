unit MainFormUnit;

interface

uses
	FrameFactoryUnit,

	Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
	Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.StdCtrls;

type
	TMainForm = class( TForm )
		Button1 : TButton;
		Button2 : TButton;
		Button3 : TButton;
		CurrentFramePanel : TPanel;
    Button4: TButton;
		procedure Button1Click( Sender : TObject );
		procedure Button2Click( Sender : TObject );
		procedure Button3Click( Sender : TObject );
		procedure Button4Click(Sender: TObject);
	private
		FCurrentFrame : Tframe;

		procedure SetCurrentFrame( const AFrame : Tframe );

	public
		property CurrentFrame : Tframe read FCurrentFrame write SetCurrentFrame;

	end;

var
	MainForm : TMainForm;

implementation

{$R *.dfm}

procedure TMainForm.Button1Click( Sender : TObject );
begin

	CurrentFrame := FrameFactory( 'TFrame1' );

end;

procedure TMainForm.Button2Click( Sender : TObject );
begin

	CurrentFrame := FrameFactory( 'TFrame2' );

end;

procedure TMainForm.Button3Click( Sender : TObject );
begin

	CurrentFrame := FrameFactory( 'TFrame3' );

end;

procedure TMainForm.Button4Click(Sender: TObject);
begin

	CurrentFrame := nil;

end;

procedure TMainForm.SetCurrentFrame( const AFrame : Tframe );
begin

	// One can always call FreeAndNil, even on an unassigned object, to destroy it. Wrapping the frame in
	// an interface using reference counting could eliminate this line as well
	FreeAndNil( FCurrentFrame );

	// Asssign the field variable
	FCurrentFrame := AFrame;

	// Do whatever needs to be done and show the frame on the CurrentFramePanel
	if Assigned( FCurrentFrame ) then
	begin

		FCurrentFrame.Parent := CurrentFramePanel;

	end;

end;

end.
